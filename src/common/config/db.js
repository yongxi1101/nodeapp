'use strict';
/**
 * db config
 * @type {Object}
 */
export default {
  type: 'mysql',
  adapter: {
    mysql: {
      host: '45.32.22.10',
      port: '3306',
      database: 'yongxi',
      user: 'code',
      password: 'hello1234',
      prefix: 'yx_',
      encoding: 'utf8'
    }
  }
};