'use strict';
/**
 * model
 */
export default class extends think.model.base {
  async getUserList(){
    return await this.model('user').select();
  }
}